import { Meteor } from 'meteor/meteor';
import { check, Match } from 'meteor/check';

/**
 * Log a server-side method call
 * @param {string} methodName - Name of method being called
 * @param {Object} params - Object with method parameter key/value pairs
 */
const logMethod = (methodName, params) => {
    console.log(`\nMethod Call: ${methodName}`);
    console.log('params:', { ...params });
};

/**
 * Throw a Meteor.Error for an HTTP error response
 * @param {Object} e - Error object returned from HTTP call
 */
const handleError = e => {
    const http_code = (e.response && e.response.data) ? e.response.data.http_code : 0;
    switch (http_code) {
        case 429:
            throw new Meteor.Error(http_code, "Too Many Requests");
        default:
            throw new Meteor.Error(http_code, "Internal Server Error");
    }
};

Meteor.methods({
    "search": function (q) {
        logMethod('search', { q });
        check(q, String);
        this.unblock();
        try {
            const result = HTTP.get(
                `http://behance.net/v2/users`,
                {
                    params: {
                        client_id: Meteor.settings.behance,
                        q
                    }
                }
            );
            return result;
        } catch (e) {
            handleError(e);
        }
    },
    "getUserInfo": function (id) {
        logMethod('getUserInfo', { id });
        check(id, Match.OneOf(Number, String));
        this.unblock();
        try {
            const result = HTTP.get(
                `http://behance.net/v2/users/${id}`,
                {
                    params: {
                        client_id: Meteor.settings.behance
                    }
                }
            );
            return result;
        } catch (e) {
            handleError(e);
        }
    },
    "getUserProjects": function (id) {
        logMethod('getUserProjects', { id });
        check(id, Match.OneOf(Number, String));
        this.unblock();
        try {
            const result = HTTP.get(
                `http://behance.net/v2/users/${id}/projects`,
                {
                    params: {
                        client_id: Meteor.settings.behance
                    }
                }
            );
            return result;
        } catch (e) {
            handleError(e);
        }
    },
    "getUserWorkExperience": function (id) {
        logMethod('getUserWorkExperience', { id });
        check(id, Match.OneOf(Number, String));
        this.unblock();
        try {
            const result = HTTP.get(
                `http://behance.net/v2/users/${id}/work_experience`,
                {
                    params: {
                        client_id: Meteor.settings.behance
                    }
                }
            );
            return result;
        } catch (e) {
            handleError(e);
        }
    },
    "getUserFollowers": function (id) {
        logMethod('getUserFollowers', { id });
        check(id, Match.OneOf(Number, String));
        this.unblock();
        try {
            const result = HTTP.get(
                `http://behance.net/v2/users/${id}/followers`,
                {
                    params: {
                        client_id: Meteor.settings.behance
                    }
                }
            );
            return result;
        } catch (e) {
            handleError(e);
        }
    },
    "getUserFollowing": function (id) {
        logMethod('getUserFollowing', { id });
        check(id, Match.OneOf(Number, String));
        this.unblock();
        try {
            const result = HTTP.get(
                `http://behance.net/v2/users/${id}/following`,
                {
                    params: {
                        client_id: Meteor.settings.behance
                    }
                }
            );
            return result;
        } catch (e) {
            handleError(e);
        }
    },
});
