import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { chai, expect } from 'meteor/practicalmeteor:chai';
import SearchUsers from '../SearchUsers';
import { spies } from 'meteor/practicalmeteor:sinon';
configure({ adapter: new Adapter() });

if (Meteor.isClient) {
    describe('SearchUsers', function () {
        const wrapper = shallow(<SearchUsers />);
        it('should call _searchBehance() with correct query on text input change ', function () {
            spies.create('searchSpy', wrapper.instance(), '_searchBehance');
            wrapper.instance()._handleSearchChange({ target: {value: 'Test'} });
            expect(spies.searchSpy).to.have.been.calledWith('Test');
        });
    });
}