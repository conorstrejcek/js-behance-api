import React from 'react';
import debounce from 'lodash.debounce';
import { Search, Message } from 'semantic-ui-react'
import './SearchUsers.less';

export default class SearchUsers extends React.Component {
    state = {
        searchText: "",
        isLoading: false,
        results: [],
        height: window.innerHeight,
        error: null
    };

    updateDimensions = debounce(() => {
        this.setState({height: window.innerHeight});
    }, 300);

    componentDidMount() {
        window.addEventListener("resize", this.updateDimensions);
    };

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions);
    };

    _handleSearchChange = e => {
        //console.log('_handleSearchChange', e);
        const searchText = e.target.value;
        this.setState({searchText, isLoading: true});
        this._searchBehance(searchText);
    };

    _searchBehance = debounce(q => {
        //console.log('_searchBehance', q);
        Meteor.call('search', q, (err, res) => {
            if (err) {
                console.log(err);
                this.setState({isLoading: false, error: err.reason});
            } else {
                const {data} = res;
                const {http_code, users} = data;
                let results = [];
                if (http_code === 200) {
                    results = users.map(u => ({
                        title: u.first_name + ' ' + u.last_name,
                        image: u.images[100],
                        description: u.username,
                        id: u.id
                    }))
                }
                this.setState({isLoading: false, results, error: null});
            }
        });
    }, 300);

    _handleResultSelect = (e, data) => {
        //console.log('selected:', data);
        FlowRouter.go(`/user/${data.result.id}`);
    };

    render() {
        const { isLoading, searchText, results, height, error } = this.state;
        const marginTop = searchText.trim().length > 0 ? 0 : (height / 2) - 65;
        return (
            <div className="container-fullscreen">
                <div className="search-container" style={{marginTop}}>
                    <h1>Search for Users</h1>
                    {error && (
                        <Message negative>
                            <Message.Header>Oops! There was an error:</Message.Header>
                            <p>{error}</p>
                        </Message>
                    )}
                    <Search
                        size="big"
                        placeholder="e.g. Ansel Adams"
                        className="search-box"
                        fluid
                        loading={isLoading}
                        onResultSelect={this._handleResultSelect}
                        onSearchChange={this._handleSearchChange}
                        results={results}
                        value={searchText}
                    />
                </div>
            </div>
        )
    }
}