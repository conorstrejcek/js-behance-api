import React from 'react';
import { Grid, Image, Loader, Statistic, Label, Icon, Item } from 'semantic-ui-react'
import './UserProfile.less';

// Taken from: https://stackoverflow.com/a/2901298/2367913
const numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

export const SocialLinks = props => (
    props.social_links.map(sl => {
        let icon;
        switch (sl.service_name) {
            case 'Instagram':
                icon = "instagram";
                break;
            case 'Facebook':
                icon = "facebook square";
                break;
            case 'LinkedIn':
                icon = "linkedin square";
                break;
            case 'Pinterest':
                icon = "pinterest square";
                break;
            case 'Tumblr':
                icon = "tumblr square";
                break;
            case 'Twitter':
                icon = "twitter square";
                break;
            case 'Vimeo':
                icon = "vimeo square";
                break;
            case "YouTube":
                icon = "youtube square";
                break;
            case "Flickr":
                icon = "flickr";
                break;
            case "Dribbble":
                icon = "dribble";
                break;
            default:
                icon = "external-link";
                break;
        }
        return (
            <a key={sl.social_id} href={sl.url}>
                <Icon size="large" name={icon}/>
            </a>
        )
    })
);

export const BasicInfo = props => (
    <div className="padding-small">
        {props.loading ? <Loader className="loader-centered" active inline="centered" /> : (
            <div>
                {props.images &&
                    <Image
                        rounded centered
                        as='a' href={props.url}
                        height={276} src={props.images[276]}
                    />
                }
                <h1>{props.name || "No Name"}</h1>
                <div className="social-links"><SocialLinks social_links={props.social_links || []} /></div>
                {props.occupation &&
                    <Label className="user-occupation label-margin-vertical label-margin-horizontal">
                        <Icon name='building outline'/> {props.occupation}
                    </Label>
                }
                {props.location &&
                    <Label className="user-location label-margin-vertical label-margin-horizontal">
                        <Icon name='marker'/> {props.location}
                    </Label>
                }
                {props.website &&
                    <a className="user-website" href={props.website}>
                        <Label className="label-margin-vertical label-margin-horizontal">
                            <Icon name='external'/> {props.website}
                        </Label>
                    </a>
                }
                {props.stats &&
                    <Statistic.Group className="user-statistics" size="mini">
                        <Statistic className="user-views">
                            <Statistic.Value>{numberWithCommas(props.stats.views)}</Statistic.Value>
                            <Statistic.Label>Views</Statistic.Label>
                        </Statistic>
                        <Statistic className="user-appreciations">
                            <Statistic.Value>{numberWithCommas(props.stats.appreciations)}</Statistic.Value>
                            <Statistic.Label>Appreciations</Statistic.Label>
                        </Statistic>
                        <Statistic className="user-followers">
                            <Statistic.Value>{numberWithCommas(props.stats.followers)}</Statistic.Value>
                            <Statistic.Label>Followers</Statistic.Label>
                        </Statistic>
                        <Statistic className="user-following">
                            <Statistic.Value>{numberWithCommas(props.stats.following)}</Statistic.Value>
                            <Statistic.Label>Following</Statistic.Label>
                        </Statistic>
                        <Statistic className="user-comments">
                            <Statistic.Value>{numberWithCommas(props.stats.comments)}</Statistic.Value>
                            <Statistic.Label>Comments</Statistic.Label>
                        </Statistic>
                    </Statistic.Group>
                }
            </div>
        )}
    </div>
);

export const Projects = props => (
    props.loading ? <Loader className="loader-centered" active inline="centered" /> : (
        <Item.Group link relaxed>
            {props.projects.map((p, index) => (
                <Item href={p.url} key={p.id} className={`item-row ${(index % 2 === 0) ? '' : 'item-dark'}`}>
                    <Item.Image src={p.covers[202]} />
                    <Item.Content verticalAlign='middle'>
                        <Item.Header className="project-name">{p.name}</Item.Header>
                        <Item.Meta className="project-fields">{p.fields.join(', ')}</Item.Meta>
                        <Item.Extra className="project-modified">Last Modified: {(new Date(p.modified_on * 1000)).toLocaleString()}</Item.Extra>
                        <Item.Description>
                            <Statistic.Group size="mini">
                                <Statistic className="project-statistic project-views">
                                    <Statistic.Value>{numberWithCommas(p.stats.views)}</Statistic.Value>
                                    <Statistic.Label>Views</Statistic.Label>
                                </Statistic>
                                <Statistic className="project-statistic project-appreciations">
                                    <Statistic.Value>{numberWithCommas(p.stats.appreciations)}</Statistic.Value>
                                    <Statistic.Label>Appreciations</Statistic.Label>
                                </Statistic>
                                <Statistic className="project-statistic project-comments">
                                    <Statistic.Value>{numberWithCommas(p.stats.comments)}</Statistic.Value>
                                    <Statistic.Label>Comments</Statistic.Label>
                                </Statistic>
                            </Statistic.Group>
                        </Item.Description>
                    </Item.Content>
                </Item>
            ))}
        </Item.Group>
    )
);

export const WorkExperience = props => (
    props.loading ? <Loader className="loader-centered" active inline="centered" /> : (
        <Item.Group divided>
            {props.work_experience.map((w, i) => (
                <Item key={`work_experience_${i}`}>
                    <Item.Content>
                        <Item.Header className="work-organization">{w.organization}</Item.Header>
                        <Item.Meta className="work-location">{w.location}</Item.Meta>
                        <Item.Description className="work-position">{w.position}</Item.Description>
                    </Item.Content>
                </Item>
            ))}
        </Item.Group>
    )
);

export const Follows = props => (
    props.loading ? <Loader className="loader-centered" active inline="centered" /> : (
        <Label.Group>
            {props.follows.slice(0, 15).map(f => (
                <Label className="follow-label label-margin-vertical" key={`${props.type}_${f.id}`} as='a' basic image href={f.url}>
                    <img className="follow-image" src={f.images[50]} />
                    {f.display_name || 'No Name'}
                </Label>
            ))}
        </Label.Group>
    )
);

export default class SearchUsers extends React.Component {
    state = {
        loadingBasicInfo: true,
        loadingProjects: true,
        loadingWorkExperience: true,
        loadingFollowers: true,
        loadingFollowing: true,
        basicInfo: {},
        projects: [],
        work_experience: [],
        followers: [],
        following: []
    };
    /**
     * Call a meteor method and handle the response
     * @param {string} methodName - Name of meteor method to call
     * @param args - Arguments to be passed to the meteor method
     * @param {string} loadingKey - Key used to handle loading indicator (with "loading" prefix stripped)
     * @param {function} success - Success callback, called with "data" as only argument
     */
    meteorMethod = (methodName, args, loadingKey, success) => {
        const error = () => this.setState({[`loading${loadingKey}`]: false});
        Meteor.call(methodName, args, (err, res) => {
            if (err) {
                error();
            } else {
                const { data } = res;
                const { http_code } = data;
                if (http_code === 200) {
                    success(data);
                } else {
                    error();
                }
            }
        });
    };
    componentWillMount() {
        const { meteorMethod, props } = this;
        const { id } = props;
        meteorMethod("getUserInfo", id, "BasicInfo",
            ({user}) => this.setState({loadingBasicInfo: false, basicInfo: {...user}})
        );
        meteorMethod("getUserProjects", id, "Projects",
            ({projects}) => this.setState({loadingProjects: false, projects}),
        );
        meteorMethod("getUserWorkExperience", id, "WorkExperience",
            ({work_experience}) => this.setState({loadingWorkExperience: false, work_experience})
        );
        meteorMethod("getUserFollowers", id, "Followers",
            ({followers}) => this.setState({loadingFollowers: false, followers})
        );
        meteorMethod("getUserFollowing", id, "Following",
            ({following}) => this.setState({loadingFollowing: false, following})
        );
    }
    render() {
        const {
            loadingBasicInfo,
            loadingProjects,
            loadingWorkExperience,
            loadingFollowers,
            loadingFollowing,
            basicInfo,
            projects,
            work_experience,
            followers,
            following
        } = this.state;
        const { display_name, images, location, occupation, stats, url, website, social_links } = basicInfo;
        return (
            <Grid stackable celled='internally' relaxed>
                <Grid.Row centered>
                    <BasicInfo
                        loading={loadingBasicInfo}
                        name={display_name}
                        images={images}
                        stats={stats}
                        location={location}
                        occupation={occupation}
                        url={url}
                        website={website}
                        social_links={social_links}
                    />
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column tablet={6} computer={6} className="profile-left" largeScreen={4}>
                        {(work_experience.length > 0 || loadingWorkExperience) ? <h2>Work Experience</h2> : <h2>No Work Experience</h2>}
                        <WorkExperience
                            loading={loadingWorkExperience}
                            work_experience={work_experience}
                        />
                        {(followers.length > 0 || loadingFollowers) ? <h2>Followers</h2> : <h2>No Followers</h2>}
                        <Follows
                            type="follower"
                            loading={loadingFollowers}
                            follows={followers}
                        />
                        {(following.length > 0 || loadingFollowing) ? <h2>Following</h2> : <h2>Following None</h2>}
                        <Follows
                            type="following"
                            loading={loadingFollowing}
                            follows={following}
                        />
                    </Grid.Column>
                    <Grid.Column tablet={10} computer={10} largeScreen={12} className="profile-right">
                        {(projects.length > 0 || loadingProjects) ? <h2>Projects</h2> : <h2>No Projects</h2>}
                        <Projects
                            loading={loadingProjects}
                            projects={projects}
                        />
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    }
}