import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { chai } from 'meteor/practicalmeteor:chai';
import { BasicInfo } from '../UserProfile';
configure({ adapter: new Adapter() });

if (Meteor.isClient) {
    describe('BasicInfo', function () {
        const info = {
            loading: false,
            name: "Test Display Name",
            images: {276: 'test.png'},
            location: "Test Location",
            occupation: "Test Occupation",
            stats: {views: 10000, appreciations: 50, followers: 10, following: 5, comments: 3},
            url: "http://test.com",
            website: "http://test.website.com",
            social_links: []
        };
        const infoWrapper = shallow(<BasicInfo {...info} />);
        it('renders a profile image', function () {
            chai.assert.equal(infoWrapper.find('Image').prop('src'), 'test.png');
        });
        it('links the profile image to behance', function () {
            chai.assert.equal(infoWrapper.find('Image').prop('href'), 'http://test.com');
        });
        it("renders user's name", function () {
            chai.assert.equal(infoWrapper.find('h1').text(), 'Test Display Name');
        });
        it("renders user's occupation", function () {
            const locationWrapper = infoWrapper.find(".user-occupation");
            chai.assert.equal(
                shallow(<div>{locationWrapper.prop('children')}</div>).text(),
                "<Icon /> Test Occupation"
            );
        });
        it("renders user's location", function () {
            const locationWrapper = infoWrapper.find(".user-location");
            chai.assert.equal(
                shallow(<div>{locationWrapper.prop('children')}</div>).text(),
                "<Icon /> Test Location"
            );
        });
        it("links to user's website", function () {
            chai.assert.equal(infoWrapper.find('.user-website').prop('href'), 'http://test.website.com');
        });
        it('renders stats', function () {
            const viewsWrapper = infoWrapper.find(".user-views");
            chai.assert.equal(
                shallow(<div>{viewsWrapper.prop('children')}</div>).html(),
                `<div><div class="value">10,000</div><div class="label">Views</div></div>`
            );
            const appreciationsWrapper = infoWrapper.find(".user-appreciations");
            chai.assert.equal(
                shallow(<div>{appreciationsWrapper.prop('children')}</div>).html(),
                `<div><div class="value">50</div><div class="label">Appreciations</div></div>`
            );
            const followersWrapper = infoWrapper.find(".user-followers");
            chai.assert.equal(
                shallow(<div>{followersWrapper.prop('children')}</div>).html(),
                `<div><div class="value">10</div><div class="label">Followers</div></div>`
            );
            const followingWrapper = infoWrapper.find(".user-following");
            chai.assert.equal(
                shallow(<div>{followingWrapper.prop('children')}</div>).html(),
                `<div><div class="value">5</div><div class="label">Following</div></div>`
            );
            const commentsWrapper = infoWrapper.find(".user-comments");
            chai.assert.equal(
                shallow(<div>{commentsWrapper.prop('children')}</div>).html(),
                `<div><div class="value">3</div><div class="label">Comments</div></div>`
            );
        });
    });
}