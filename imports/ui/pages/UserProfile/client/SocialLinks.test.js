import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { chai } from 'meteor/practicalmeteor:chai';
import { SocialLinks } from '../UserProfile';
configure({ adapter: new Adapter() });

if (Meteor.isClient) {
    describe('SocialLinks', function () {
        const social_links = [
            {social_id: 1, url: "http://twitter.com/strangerstuff", service_name: "Twitter", value: "strangerstuff"},
            {social_id: 2, url: "http://facebook.com/StrangerAndStranger", service_name: "Facebook", value: "StrangerAndStranger"},
            {social_id: 4, url: "http://linkedin.com/company/stranger-&-stranger", service_name: "LinkedIn", value: "company/stranger-&-stranger"},
            {social_id: 6, url: "http://flickr.com/strangerandstranger", service_name: "Flickr", value: "strangerandstranger"},
            {social_id: 7, url: "http://youtube.com/Morganstranger", service_name: "YouTube", value: "Morganstranger"},
            {social_id: 8, url: "http://pinterest.com/strangerpin", service_name: "Pinterest", value: "strangerpin"},
            {social_id: 12, url: "http://instagram.com/strangerandstranger", service_name: "Instagram", value: "strangerandstranger"}
        ];
        const wrapper = shallow(<div><SocialLinks social_links={social_links} /></div>);
        it('renders correct icons and urls', function () {
            chai.assert.equal(
                wrapper.html(),
                `<div><a href="http://twitter.com/strangerstuff"><i aria-hidden="true" class="twitter square large icon"></i></a><a href="http://facebook.com/StrangerAndStranger"><i aria-hidden="true" class="facebook square large icon"></i></a><a href="http://linkedin.com/company/stranger-&amp;-stranger"><i aria-hidden="true" class="linkedin square large icon"></i></a><a href="http://flickr.com/strangerandstranger"><i aria-hidden="true" class="flickr large icon"></i></a><a href="http://youtube.com/Morganstranger"><i aria-hidden="true" class="youtube square large icon"></i></a><a href="http://pinterest.com/strangerpin"><i aria-hidden="true" class="pinterest square large icon"></i></a><a href="http://instagram.com/strangerandstranger"><i aria-hidden="true" class="instagram large icon"></i></a></div>`
            );
        });
    });
}