import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { chai } from 'meteor/practicalmeteor:chai';
import { WorkExperience } from '../UserProfile';
configure({ adapter: new Adapter() });

if (Meteor.isClient) {
    describe('WorkExperience', function () {
        const work_experience = [
            {
                "position": "Co-Founder & Chief of Design",
                "organization": "Behance",
                "location": "Brooklyn, NY, USA"
            },
            {
                "position": "Senior Designer",
                "organization": "AR Media",
                "location": "Brooklyn, NY, USA"
            },
            {
                "position": "Senior Designer",
                "organization": "Pro Am",
                "location": "New York, NY, USA"
            },
            {
                "position": "Junior Designer",
                "organization": "Zeligmedia",
                "location": "Barcelona, Spain"
            },
            {
                "position": "Junior Designer",
                "organization": "Triada Comunicacion",
                "location": "Barcelona, Spain"
            }
        ];
        const wrapper = shallow(<WorkExperience work_experience={work_experience} />);
        it('renders all work experience', function () {
            chai.assert.equal(
                wrapper.find("Item").length,
                5
            );
        });
        it('renders organization', function () {
            const headerWrapper = wrapper.find(".work-organization").first();
            chai.assert.equal(
                shallow(<div>{headerWrapper.prop('children')}</div>).text(),
                "Behance"
            );
        });
        it('renders location', function () {
            const headerWrapper = wrapper.find(".work-location").first();
            chai.assert.equal(
                shallow(<div>{headerWrapper.prop('children')}</div>).text(),
                "Brooklyn, NY, USA"
            );
        });
        it('renders position', function () {
            const headerWrapper = wrapper.find(".work-position").first();
            chai.assert.equal(
                shallow(<div>{headerWrapper.prop('children')}</div>).text(),
                "Co-Founder & Chief of Design"
            );
        });
    });
}