# Dealer Inspire Front End Code Challenge

## Technology Stack

- Frontend Frameworks: Meteor / ReactJS / Semantic UI React
- Backend Frameworks:  Meteor
- Package Managers:    Atmosphere / NPM
- Testing Frameworks:  Enzyme / Mocha / Chai

## Getting Started

1. Install Meteor with `curl https://install.meteor.com/ | sh` on a MacOS device.
2. Run `meteor npm i` within the root of the repo to install all npm packages.
3. Create a `settings.json` file in the root of the repo with the following: `{ "behance": "yourapikeyhere" }`
4. Run `npm run start` to start Meteor on port `3000`. You can view the application in the browser at `localhost:3000`.
5. (Optionally) run `meteor test --driver-package practicalmeteor:mocha --port 3100` to start the test-runner on port `3100`. You can view the output in the browser at `localhost:3100`.

## Notes

I have tested the application on my MacOS environment, but I am not sure how the project will run on Windows. You can install meteor on Windows using [Chocolatey](https://chocolatey.org/install) by running `choco install meteor`, but I haven't tried it myself.